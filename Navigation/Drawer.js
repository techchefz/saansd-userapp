//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, Image } from 'react-native';
import { createDrawerNavigator } from "react-navigation";

import EIcon from "react-native-vector-icons/EvilIcons";
import INcon from "react-native-vector-icons/Ionicons";
import EnIcon from "react-native-vector-icons/Entypo";
import MIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SIcon from "react-native-vector-icons/SimpleLineIcons";

import Home from "../Component/homeTwo";
import Complaints from "../Component/complaints";
import ContactUs from "../Component/contactUs";
import Language from "../Component/language";
import Profile from "../Component/profile";
import Poll from "../Component/poll";
import Schedule from "../Component/schedule";
import Donation from "../Component/donation";
import Journey from "../Component/journey";
import Achievement from "../Component/achievement";
import News from "../Component/news";

class MyHomeScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Home',
        drawerIcon: ({ tintColor }) => (
            <INcon
                name="ios-home"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Home navigation={this.props.navigation} />
        );
    }
}

class MyProfile extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Profile',
        drawerIcon: ({ tintColor }) => (
            <EIcon
                name="user"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Profile navigation={this.props.navigation} />
        );
    }
}

class MyComplaints extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Complaints',
        drawerIcon: ({ tintColor }) => (
            <SIcon
                name="note"
                size={20}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Complaints navigation={this.props.navigation} />
        );
    }
}

class MyContactUs extends React.Component {
    static navigationOptions = {
        drawerLabel: 'ContactUs',
        drawerIcon: ({ tintColor }) => (
            <INcon
                name="ios-call"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <ContactUs navigation={this.props.navigation} />
        );
    }
}


class MyPoll extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Poll',
        drawerIcon: ({ tintColor }) => (
            <MIcon
                name="poll"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Poll navigation={this.props.navigation} />
        );
    }
}

class MySchedule extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Schedule',
        drawerIcon: ({ tintColor }) => (
            <MIcon
                name="calendar-clock"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Schedule navigation={this.props.navigation} />
        );
    }
}

// class MyDonation extends React.Component {
//     static navigationOptions = {
//         drawerLabel: 'Donation',
//         drawerIcon: ({ tintColor }) => (
//             <Image
//                 source={require("../assets/donation.png")}
//                 style={{
//                     tintColor: "#fff",
//                     width: 30,
//                     height: 30,
//                     resizeMode: "contain"
//                 }} />
//         ),
//     };

//     render() {
//         return (
//             <Donation navigation={this.props.navigation} />
//         );
//     }
// }

class MyJourney extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Journey',
        drawerIcon: ({ tintColor }) => (
            <INcon
                name="md-walk"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Journey navigation={this.props.navigation} />
        );
    }
}

class MyAchievement extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Achievement',
        drawerIcon: ({ tintColor }) => (
            <INcon
                name="md-walk"
                size={30}
                color="#fff" />
        ),
    };

    render() {
        return (
            <Achievement navigation={this.props.navigation} />
        );
    }
}

class MyNews extends React.Component {
    static navigationOptions = {
        drawerLabel: 'News',
        drawerIcon: ({ tintColor }) => (
            <EnIcon
                name="users"
                size={25}
                color="#fff" />
        ),
    };

    render() {
        return (
            <News navigation={this.props.navigation} />
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        width: 24,
        height: 24,
    },
});

export const MyApp = createDrawerNavigator({
    Home: { screen: MyHomeScreen },
    News: { screen: MyNews },
    Profile: { screen: MyProfile },
    Poll: { screen: MyPoll },
    Schedule: { screen: MySchedule },
    // Donation: { screen: MyDonation },
    Journey: { screen: MyJourney },
    Achievement: { screen: MyAchievement },
    Complaints: { screen: MyComplaints, },
    ContactUs: { screen: MyContactUs },

}, {
        contentOptions: {
            inactiveTintColor: '#fff',
            activeTintColor: '#fff'
        },
        drawerBackgroundColor: '#FF5721',
    });

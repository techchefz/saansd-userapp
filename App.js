import React from 'react';
import { StyleSheet, Text, View, Platform, StatusBar } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { Provider } from "react-redux";
import configureStore from "./redux/configureStore";
import { Root } from "native-base";
import Splash from "./Component/splashScreen";
import Login from "./Component/login";
import Signup from "./Component/signup";
import { MyApp } from "./Navigation/Drawer";
import { Font, AppLoading } from "expo";
import Card from "./Component/cardPayment";
import Payment from "./Component/paymentPage";
import NetBanking from "./Component/netBanking";
import PaymentSuccess from "./Component/paymentSuccess";

const store = configureStore()

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
      <Provider store={store}>
        <Root>
          <RootStack />
        </Root>
      </Provider>
    );
  }
}
const RootStack = createStackNavigator({
  Login: { screen: Login },
  SignUp: { screen: Signup },
  Splash: { screen: Splash },
  MyDrawer: {
    screen: MyApp,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
  CardPayment: { screen: Card },
  PaymentPage: { screen: Payment },
  netBanking: { screen: NetBanking },
  paymentSuccess: { screen: PaymentSuccess },
},
  {
    headerMode: 'none'
  }
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
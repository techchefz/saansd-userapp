//import liraries
import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, Image } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, Spinner } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
import Menu from "./ExpandableView/Aapp";
import { fetchAchievementData } from '../redux/reducers/Achievement/actions';
import { connect } from 'react-redux';
import { AdMobBanner } from "expo";
// create a component
class Achievement extends Component {
    
    constructor(props) {
        super(props)

        this.state = {
            achievementData: null,
            loader: true
        }
    }

    componentWillMount() {
        this.props.onFetchAchievementData()
        if (this.state.achievementData == null ) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.Data.achievement.data !== undefined) {
            this.setState({
                loader: false,
                achievementData: nextProps.Data.achievement
            })
        }
    }
    
    render() {
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Achievement</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                {this.state.loader == true ? 
                <Spinner/>
                :
                <Content style={{ width: "100%", height: "100%", flex: 1, padding: 10 }}>
                    {/* <Menu /> */}
                    <Image
                    source={{uri: this.state.achievementData.data.image}}
                    style={{width:"100%", height:200, resizeMode:"contain"}}/>
                    <Text style={{
                        fontSize: 16,
                    }}>
                       {this.state.achievementData.data.achievement} 
                        </Text>
                    <View style={{width:"100%", height:50}}/>
                </Content>
                }
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onFetchAchievementData: () => {
            dispatch(fetchAchievementData());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Achievement);

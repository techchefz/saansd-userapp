import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { Spinner } from "native-base";
import { fetchFeaturedImageData } from '../redux/reducers/FeaturedImage/actions';
import { connect } from 'react-redux';

class Slideshow extends Component {

    constructor(props) {
        super(props);

        this.state = {
            position: 1,
            interval: null,
            imageData: [],
            loader: true
        };
    }

    componentWillMount() {
        this.props.onFeaturedImage()
        if (this.state.imageData.length == 0) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
        this.setState({
            interval: setInterval(() => {
                this.setState({ position: this.state.position === 2 ? 0 : this.state.position + 1 });
            }, 3000)
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.Data.featuredImage.data !== null) {
            this.setState({
                loader: false,
                imageData: nextProps.Data.featuredImage
            })
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    render() {
        return (
            this.state.loader == true ? 
                <Spinner/>
                :
                <ImageSlider images={[
                    
                    this.state.imageData.data.data[0],
                    this.state.imageData.data.data[1],
                    this.state.imageData.data.data[2]
                ]}
                    position={this.state.position}
                    height= '100%'
                    onPositionChanged={position => this.setState({ position })} />
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onFeaturedImage: () => {
            dispatch(fetchFeaturedImageData());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Slideshow);
//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView, TouchableOpacity, Platform, Linking } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Spinner, Content } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
import { fetchContactData } from '../redux/reducers/ContactUs/actions';
import { socialLinkData } from "../redux/reducers/socialLinks/actions";
import { connect } from 'react-redux';
import MapView from "react-native-maps";
import { AdMobBanner } from "expo";
// create a component
class ContactUs extends Component {
    constructor(props) {
        super(props)

        this.state = {
            contactData: null,
            socialLinks: null,
            loader: true,
            loaderLink: true,
            youtube: null,
            facebook: null,
            twitter: null,
            instagram:null,
        }
    }
    componentWillMount() {
        this.props.onContact();
        this.props.onsocialLinkData();
        if (this.state.contactData == null) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
        if (this.state.socialLinks == null) {
            this.setState({
                loaderLink: true
            })
        } else {
            this.setState({
                loaderLink: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        let contactData = null;
        if (nextProps.Data.contactUs !== undefined || null) {
            if (nextProps.Data.contactUs.data.length !== 0) {
                nextProps.Data.contactUs.data.forEach(data => {
                    this.setState({
                        loader: false,
                        contactData: data
                    })
                })
            }
        }
        if (nextProps.Data.socialLinks.data !== null || undefined) {
            nextProps.Data.socialLinks.data.data.forEach(dataLinks => {
                this.setState({
                    youtube: dataLinks.youtubeLink,
                    facebook: dataLinks.facebookLink,
                    twitter: dataLinks.twitterLink,
                    instagram: dataLinks.instagramLink,
                })
            })
        }
    }
    render() {
        // console.log('====================================')
        // console.log(this.state.socialLinks)
        // console.log('====================================')
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }}> */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10, flex: 1 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body style={{ flex: 2 }}>
                        <Title style={{ color: "#fff" }}>Contact Us</Title>
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                {/* </SafeAreaView> */}
                {this.state.loader == true ?
                    <Spinner />
                    :
                    <View style={{ flex: 1, width: "100%" }}>
                        <Content style={{ flex: 1, width: "100%" }}>
                            <View style={{ flex: 1, width: "100%", height: 200 }}>
                                {/* <Image
                                source={require('../assets/map.png')}
                                style={{
                                    height: "100%",
                                    width: "100%"
                                }} /> */}
                                <MapView style={styles.map}
                                    let latitudeData={this.state.contactData.latitude}
                                    let longitudeData={this.state.contactData.longitude}
                                    region={{
                                        latitude: this.state.contactData.latitude,
                                        longitude: this.state.contactData.longitude,
                                        latitudeDelta: 0.1,
                                        longitudeDelta: 0.1,
                                    }}
                                >
                                    <MapView.Marker
                                        coordinate={{
                                            latitude: this.state.contactData.latitude,
                                            longitude: this.state.contactData.longitude,
                                        }}
                                        title={'Our office'}
                                    />
                                </MapView>
                            </View>

                            <View
                                style={{
                                    flex: 1.5,
                                    padding: 20
                                    // justifyContent: "center",
                                    // alignItems: "center"
                                }}>
                                <Text style={{ fontSize: 20, textAlign: "left" }}>
                                    {this.state.contactData.address}
                                </Text>
                                <Text style={{ fontSize: 20, textAlign: "left", marginTop: 25 }}>
                                    LandlineNo : {this.state.contactData.landlineNo} {'\n'}
                                    FaxNo :{this.state.contactData.faxNo} {"\n"}
                                    Email : {this.state.contactData.email}
                                </Text>
                            </View>
                        </Content>
                        <View style={{ padding: 5, flexDirection: "row", width: "100%", justifyContent: "space-around" }}>
                            <TouchableOpacity
                                onPress={() => Linking.openURL(this.state.facebook)}
                                style={styles.socialIcon}>
                                <Image
                                    source={require('../assets/facebook.png')}
                                    style={{ width: 50, height: 50 }}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Linking.openURL(this.state.instagram)}
                                style={styles.socialIcon}>
                                <Image
                                    source={require('../assets/instagram.png')}
                                    style={{ width: 50, height: 50 }}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Linking.openURL(this.state.youtube)}
                                style={styles.socialIcon}>
                                <Image
                                    source={require('../assets/youtube.png')}
                                    style={{ width: 50, height: 50 }}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Linking.openURL(this.state.twitter)}
                                style={styles.socialIcon}>
                                <Image
                                    source={require('../assets/twitter.png')}
                                    style={{ width: 50, height: 50 }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#fff',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    socialIcon: {
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 1
            },
            android: { elevation: 4 }
        })
    }
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onContact: () => {
            dispatch(fetchContactData());
        },
        onsocialLinkData: () => {
            dispatch(socialLinkData());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state,
        socialLink: state.socialLinks.data.data[0]
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);

//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, Form, Item, Input, Label } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";

// create a component
class PaymentPage extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10, flex: 1 }}>
                        <IIcon
                            onPress={() => this.props.navigation.goBack()}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body style={{ flex: 2 }}>
                        <Title style={{ color: "#fff" }}>Choose pay method</Title>
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                <Content style={{ width: "100%", }}>
                    <View style={{ marginTop: 10, padding: 5, paddingRight: 20 }}>
                        <Form>
                            <Item floatingLabel>
                                <Label>Name</Label>
                                <Input />
                            </Item>
                            <Item floatingLabel>
                                <Label>Mobile</Label>
                                <Input
                                    keyboardType="phone-pad" />
                            </Item>
                        </Form>
                    </View>
                    <View style={{ height: 30, justifyContent: 'center', paddingLeft: 15, marginTop: 20 }} >
                        <Text style={{ color: '#262626' }}>NET BANKING</Text>
                    </View>
                    <View style={{ backgroundColor: 'white', width: "100%", elevation: 2, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-around', margin: 5, height: Dimensions.get('window').height * (1 / 10) }}>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('netBanking')}
                        style={{height:"100%", width:"10%"}}>
                            <Image
                            source={{ uri: 'https://4.bp.blogspot.com/-icFz8IyLJoA/WifEAauZ2QI/AAAAAAAACa0/yHC8Bxk1I20CyEoVAoUG9ByqUbmGOMOBQCLcBGAs/s1600/icici.PNG' }}
                            style={{ height: '100%', width: '100%', resizeMode: "contain" }}
                            resizeMode='contain'
                        />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('netBanking')}
                        style={{height:"100%", width:"10%"}}>
                            <Image
                            source={{ uri: 'https://1.bp.blogspot.com/-d88fDwuxbnk/WifEAb5cuzI/AAAAAAAACaw/vtAv1AID-PErmI9yC-54Q2xnDHdIpGOdwCLcBGAs/s1600/hdfc.PNG' }}
                            style={{ height: '100%', width: '100%', resizeMode: "contain" }}
                            resizeMode='contain'
                        />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('netBanking')}
                        style={{height:"100%", width:"10%"}}>
                            <Image
                            source={{ uri: 'https://3.bp.blogspot.com/-J_8uZdJVGU0/WifEBgCy-pI/AAAAAAAACbA/2Ev_zLtxVB89LKtQfvXwabOO4ICJU-jYwCLcBGAs/s1600/kotak.PNG' }}
                            style={{ height: '100%', width: '100%', resizeMode: "contain" }}
                            resizeMode='contain'
                        />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('netBanking')}
                        style={{height:"100%", width:"10%"}}>
                            <Image
                            source={{ uri: 'https://1.bp.blogspot.com/-7X2Dqi_rRwg/WifEAXyJjrI/AAAAAAAACa4/IaOZkyGYGTIWY97V7oSRf2fNV5Ewk6MvgCLcBGAs/s1600/axis.PNG' }}
                            style={{ height: '100%', width: '100%', resizeMode: "contain" }}
                            resizeMode='contain'
                        />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('netBanking')}
                        style={{height:"100%", width:"10%"}}>
                            <Image
                            source={{ uri: 'https://1.bp.blogspot.com/-h4iOgfEeSJE/WifEBqmvKYI/AAAAAAAACa8/yHFWXJvnyhUM9QccDjqjq4CW63lZxF4CgCLcBGAs/s1600/sbi.PNG' }}
                            style={{ height: '100%', width: '100%', resizeMode: "contain" }}
                            resizeMode='contain'
                        />
                        </TouchableOpacity>
                    </View>
                    {/* ======================================================================= */}
                    <View style={{ backgroundColor: 'white', elevation: 2, margin: 5, flexDirection: 'row', padding: 20, height: Dimensions.get('window').height * (2 / 10) }}>
                        <View style={{ flex: 1, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('CardPayment')}
                                style={{ flex: 1, height: '100%', width: '60%', justifyContent: 'center', alignItems: 'center' }}>
                                <Image
                                    source={{ uri: 'http://www.worldoffshorebanks.com/images/prepaid-debit-card.jpg' }}
                                    style={{ height: '100%', width: '80%', }}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>
                            <Text style={{ color: '#737373' }}>Debit/Credit Card</Text>
                        </View>
                        {/* <View style={{ flex: 1, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <Image source={{ uri: 'http://www.randelltiongson.com/wp-content/uploads/2016/05/CreditCard.png' }}
                  style={{ height: '100%', width: '80%', }}
                  resizeMode='contain' />
                <Text style={{ color: '#737373' }}>Debit Card</Text>
              </View> */}
                    </View>
                </Content>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default PaymentPage;

//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Platform, TouchableOpacity, ScrollView, Image } from 'react-native';
import EIcon from "react-native-vector-icons/EvilIcons";
import INcon from "react-native-vector-icons/Ionicons";
import EnIcon from "react-native-vector-icons/Entypo";
import MIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SIcon from "react-native-vector-icons/SimpleLineIcons";
import { Container, Header, Left, Body, Right, Button, Title } from 'native-base';



// create a component
class HomeOne extends Component {
    render() {
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left />
                    <Body>
                        <Title style={{ color: "#fff" }}>Home</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                <ScrollView style={{ flex: 1, width: "100%" }}>
                    <View style={{
                        flexDirection: "row",
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        flexWrap: "wrap",
                        width: "100%"
                    }}>
                        <TouchableOpacity style={styles.box}>
                            <EnIcon
                                name="users"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>News</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Donation')}
                            style={styles.box}>
                            <Image
                                source={require("../assets/donation.png")}
                                style={{
                                    tintColor: "#FF5721",
                                    width: 40,
                                    height: 40,
                                    resizeMode: "contain"
                                }} />
                            <Text style={styles.boxTitle}>Donation</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Profile')}
                            style={styles.box}>
                            <EIcon
                                name="user"
                                size={60}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Profile</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Journey')}
                            style={styles.box}>
                            <INcon
                                name="md-walk"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Journey</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Poll')}
                            style={styles.box}>
                            <MIcon
                                name="poll"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Poll</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Schedule')}
                            style={styles.box}>
                            <MIcon
                                name="calendar-clock"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Schedule</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Complaints')}
                            style={styles.box}>
                            <SIcon
                                name="note"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Complaints</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('ContactUs')}
                            style={styles.box}>
                            <INcon
                                name="ios-call"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Contact Us</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#fff',
    },
    box: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        margin: 5,
        ...Platform.select({
            ios: {
                shadowColor: '#afafaf',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 1
            },
            android: { elevation: 4 }
        }),
        backgroundColor: "#fff"
    },
    boxTitle: {
        color: "#FF5721",
        fontSize: 18,
        fontWeight: "bold"
    }
});

//make this component available to the app
export default HomeOne;

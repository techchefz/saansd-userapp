//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Platform, TouchableOpacity, ScrollView, Dimensions, Image, Linking, AsyncStorage } from 'react-native';
import EIcon from "react-native-vector-icons/EvilIcons";
import INcon from "react-native-vector-icons/Ionicons";
import IFcon from "react-native-vector-icons/Feather";
import EnIcon from "react-native-vector-icons/Entypo";
import MIcon from "react-native-vector-icons/MaterialCommunityIcons";
import SIcon from "react-native-vector-icons/SimpleLineIcons";
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import Slider from "./ImageSlider";
import { socialLinkData } from "../redux/reducers/socialLinks/actions";
import { connect } from 'react-redux';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded
} from 'expo';
// create a component
class HomeOne extends Component {
    constructor(props) {
        super(props);

        this.state = {
            videoInfo: [],
            FaceBookInfo: [],
            youtubeChannelId: 'UC_5HPOWK_SDqV5FszV7HS7A',
            youtube: null,
            facebook: null,
            twitter: null,
            instagram:null,
            loader:true
        };
        this.VideoList = this.VideoList.bind(this);
    }

    VideoList() {
        fetch(`https://www.googleapis.com/youtube/v3/search?key=AIzaSyAp-3SyOKF7ZKP8hK7pThCCbkaQLHJA40w&channelId=${this.state.youtubeChannelId}&part=snippet,id&order=date&maxResults=2`)
            .then(resp => resp.json())
            .then((resp) => {
                //this.setState({video: resp.results});
                this.setState({ videoInfo: resp.items });
            });
    }
    FaceBookList() {
        fetch('https://graph.facebook.com/narendramodi/posts?fields=full_picture,picture,link,message,created_time&limit=2&access_token=EAAJnHiLsyw0BADxMGz1wDrTElPlTmB6qEe0DKlK1a1Tw1IZBumLLq2FhSgZA8W2ybjedTHMdXMoNSrSyFZABIKAGRPxTsYXMQrfdDjW9EYBZBsraTrL7Wy6UMX72eLvY0ETLHIK7YoTUsfLEOqqZAuW0ZABUoMYPq3eoa5NQf1L48pRsTYuEXerBzpX1hrfIgZD')
            .then(resp => resp.json())
            .then((resp) => {
                //this.setState({video: resp.results});
                this.setState({ FaceBookInfo: resp.data });
            });
    }

    componentWillMount() {
        this.props.onsocialLinkData();
        // this.VideoList();
        // this.FaceBookList();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.Data.socialLinks.data !== null || undefined) {
            nextProps.Data.socialLinks.data.data.forEach(dataLinks => {
                this.setState({
                    youtube: dataLinks.youtubeLink,
                    facebook: dataLinks.facebookLink,
                    twitter: dataLinks.twitterLink,
                    instagram: dataLinks.instagramLink,
                })
            })
        }
    }

    render() {
        console.log('====================================');
        console.log(this.state.facebook);
        console.log('====================================');
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width:"100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left>
                        <INcon name="ios-menu"
                            onPress={() => this.props.navigation.openDrawer()}
                            size={26}
                            style={{ marginLeft: 10 }}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Home</Title>
                    </Body>
                    <Right>
                        <TouchableOpacity
                            onPress={() => { AsyncStorage.clear(), this.props.navigation.popToTop() }}>
                            <IFcon name="power"
                                size={22} color="#FFF" />
                        </TouchableOpacity>
                        {/* <Text >logout</Text> */}
                    </Right>
                </Header>
                {/* </SafeAreaView> */}
                <ScrollView style={{ flex: 1, width: "100%" }}>
                    <View style={{
                        width: "100%",
                        height: Dimensions.get('screen').height * (1 / 3.5)
                    }}>
                        <Slider />
                    </View>

                    <View style={{
                        paddingTop: 0,
                        flexDirection: "row",
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        flexWrap: "wrap",
                        width: "100%",
                        marginTop: 20
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            width: "100%",
                            justifyContent: "center"
                        }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Profile')}
                                style={styles.box}>
                                <EIcon
                                    name="user"
                                    size={40}
                                    color="#FF5721" />
                                <Text style={styles.boxTitle}>Profile</Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Journey')}
                            style={styles.box}>
                            <INcon
                                name="md-walk"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Journey</Text>
                        </TouchableOpacity> */}
                            {/* <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('News')}
                            style={styles.box}>
                            <EnIcon
                                name="users"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Follow Us</Text>
                        </TouchableOpacity> */}
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Poll')}
                                style={styles.box}>
                                <MIcon
                                    name="poll"
                                    size={40}
                                    color="#FF5721" />
                                <Text style={styles.boxTitle}>Poll</Text>
                            </TouchableOpacity>
                        </View>
                        {/* <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Complaints')}
                            style={styles.box}>
                            <SIcon
                                name="note"
                                size={40}
                                color="#FF5721" />
                            <Text style={styles.boxTitle}>Complaints</Text>
                        </TouchableOpacity> */}
                        <View style={{
                            flexDirection: 'row',
                            width: "100%",
                            justifyContent: "center"
                        }}>
                            {/* <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Donation')}
                                style={styles.box}>
                                <Image source={require('../assets/RupeeIcon.png')}
                                    style={{ height: 30, width: 30, resizeMode: "contain", tintColor: "#FF5721", marginRight: 5 }} />
                                <Text style={styles.boxTitle}>Donation</Text>
                            </TouchableOpacity> */}
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Schedule')}
                                style={styles.box}>
                                <MIcon
                                    name="calendar-clock"
                                    size={40}
                                    color="#FF5721" />
                                <Text style={styles.boxTitle}>Schedule</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ContactUs')}
                                style={styles.box}>
                                <INcon
                                    name="ios-call"
                                    size={40}
                                    color="#FF5721" />
                                <Text style={styles.boxTitle}>Contact Us</Text>
                            </TouchableOpacity>
                            {/* <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Schedule')}
                                style={styles.box}>
                                <MIcon
                                    name="calendar-clock"
                                    size={40}
                                    color="#FF5721" />
                                <Text style={styles.boxTitle}>Schedule</Text>
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ width: "100%", height: 10 }} />
                    </View>

                    <View style={{ padding: 5, flexDirection: "row", width: "100%", justifyContent: "space-around", marginTop:60 }}>
                        <TouchableOpacity
                            onPress={() => Linking.openURL(this.state.facebook)}
                            style={styles.socialIcon}>
                            <Image
                                source={require('../assets/facebook.png')}
                                style={{ width: 50, height: 50 }}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => Linking.openURL(this.state.instagram)}
                            style={styles.socialIcon}>
                            <Image
                                source={require('../assets/instagram.png')}
                                style={{ width: 50, height: 50 }}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => Linking.openURL(this.state.youtube)}
                            style={styles.socialIcon}>
                            <Image
                                source={require('../assets/youtube.png')}
                                style={{ width: 50, height: 50 }}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => Linking.openURL(this.state.twitter)}
                            style={styles.socialIcon}>
                            <Image
                                source={require('../assets/twitter.png')}
                                style={{ width: 50, height: 50 }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        width: "100%",
                        padding: 10
                    }}>
                        {/* <Text style={{ fontSize: 16, fontWeight: "bold", color: "#666666", marginBottom: 10 }}>Youtube Feeds</Text> */}
                        {/* {this.state.videoInfo.map((video, i) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => Linking.openURL('https://www.youtube.com/watch?v=' + video.id.videoId)}
                                    key={i}
                                    style={{
                                        width: "100%",
                                        height: 100,
                                        borderRadius: 2,
                                        padding: 5,
                                        borderWidth: 0.5,
                                        borderColor: "#e4e4e4",
                                        marginBottom: 10,
                                        flexDirection: "row",
                                        elevation: 1
                                    }}>
                                    <View style={{ flex: 2, justifyContent: "center", alignItems: "center", padding: 1 }}>
                                        <Image source={{ uri: video.snippet.thumbnails.medium.url }}
                                            style={{ height: "100%", width: "100%" }} />
                                        <Image
                                            source={require('../assets/ytLogo.png')}
                                            style={{ height: 30, width: 30, resizeMode: "contain", position: "absolute", top: 0, left: 0 }} />
                                    </View>
                                    <View style={{ flex: 6, padding: 2 }}>
                                        <Text numberOfLines={2} style={{ fontWeight: "bold" }}>{video.snippet.title} </Text>
                                        <Text
                                            numberOfLines={3}
                                            style={{ fontSize: 12, }}>Description : <Text
                                                style={{ fontSize: 10, paddingLeft: 1, textAlign: "left" }}> {video.snippet.description} </Text></Text>

                                    </View>
                                </TouchableOpacity>
                            )
                        })} */}
                        {/* {this.state.FaceBookInfo.map((video, j) => {
                            return(
                            <TouchableOpacity
                                onPress={() => Linking.openURL(video.link)}
                                key={j}
                                style={{
                                    width: "100%",
                                    height: 100,
                                    borderRadius: 2,
                                    padding: 5,
                                    borderWidth: 0.5,
                                    borderColor: "#e4e4e4",
                                    marginBottom: 10,
                                    flexDirection: "row",
                                    elevation: 1
                                }}>
                                <View style={{ flex: 2, justifyContent: "center", alignItems: "center", padding: 1 }}>
                                    <Image source={{ uri: video.full_picture }}
                                        style={{ height: "100%", width: "100%" }} />
                                    <Image
                                        source={require('../assets/fbLogo.png')}
                                        style={{ height: 30, width: 30, resizeMode: "contain", position: "absolute", top: 0, left: 0 }} />
                                </View>
                                <View style={{ flex: 6, padding: 2 }}>
                                    <Text
                                        numberOfLines={5}
                                        style={{ fontSize: 12, fontWeight: "500" }}>{video.message}</Text>

                                </View>
                            </TouchableOpacity>
                            )
                            })} */}
                    </View>
                </ScrollView>
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#fff',
    },
    box: {
        justifyContent: 'flex-start',
        marginBottom: 10,
        alignItems: 'center',
        flexDirection: "row",
        paddingLeft: 10,
        height: 50,
        width: "45%",
        margin: 5,
        ...Platform.select({
            ios: {
                shadowColor: '#afafaf',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 1
            },
            android: { elevation: 4 }
        }),
        backgroundColor: "#fff"
    },
    boxTitle: {
        color: "#FF5721",
        fontSize: 18,
        fontWeight: "bold"
    },
    socialIcon:{
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 1
            },
            android: { elevation: 4 }
        })
    }
});

//make this component available to the app

function mapDispatchToProps(dispatch) {
    return {
        onsocialLinkData: () => {
            dispatch(socialLinkData());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state,
        // socialLink: state.socialLinks.data.data[0]
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeOne);

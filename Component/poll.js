//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Platform, Dimensions } from 'react-native';
import PieChart from 'react-native-pie-chart';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { Container, Header, Left, Body, Right, Button, Title, Content, Spinner, Item } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
import { fetchPoll } from '../redux/reducers/Polls/actions';
import { connect } from 'react-redux';
import config from "../config";
import axios from "axios";
import { AdMobBanner } from "expo";
// create a component
class Poll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            question: null,
            option: null,
            optionA: null,
            optionB: null,
            optionC: null,
            optionD: null,
            pollData: null,
            loader: true,
            responseA: 1,
            responseB: 1,
            responseC: 1,
            responseD: 1,
            polls: []
        }
    }
    componentWillMount() {
        this.props.onFetchPoll()
        if (this.state.pollData == null) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log('================nextProps====================');
        console.log(nextProps.Data.polls.data.options[0]);
        console.log('================nextProps====================');
        if (nextProps.Data.polls.data != null || undefined) {
            this.setState({
                loader: false,
                id: nextProps.Data.polls.data._id,
                question: nextProps.Data.polls.data.question,
                polls: nextProps.Data.polls.data.options,
                responseA: nextProps.Data.polls.data.responseA,
                responseB: nextProps.Data.polls.data.responseB,
                responseC: nextProps.Data.polls.data.responseC,
                responseD: nextProps.Data.polls.data.responseD,
            })
            // nextProps.Data.polls.data.options.forEach((data) => {
            //     console.log('====================================')
            //     console.log(data)
            //     console.log('====================================')
            // })

            // nextProps.Data.polls.data.forEach(data => {
            //     if (data.status == true) {
            //         this.state.polls.push(data)
            //         this.setState({
            //             loader: false
            //         })
            //     }
            // })
        }
    }
    onSelect(index, value) {
        this.setState({
            option: `${value}`
        })
    }
    submitPoll() {
        const userData = {
            _id: this.state.id,
            response: this.state.option
        }
        axios.post(`${config.serverUrl}:${config.port}/node/api/users/saveResponse`, userData).then((res) => {
            if (res.status === 200) {
                // alert("We appriciate your response");
                this.props.onFetchPoll()
            } else {
                alert("Error");
            }
        })
    }
    render() {
        console.log('====================================')
        console.log(this.state.polls)
        console.log('====================================')
        const chart_wh = 250

        const series = [
            this.state.responseA,
            this.state.responseB,
            this.state.responseC,
            this.state.responseD,
        ]
        const sliceColor = ['#F44336', '#2196F3', '#FFEB3B', '#4CAF50']
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Poll</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                {this.state.loader == true ?
                    <Spinner />
                    :
                    <Content showsVerticalScrollIndicator={false}>
                        <View style={{ flex: 1, width: Dimensions.get("screen").width }}>
                            <View style={{
                                flex: 1,
                                width: "100%",
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginBottom: 20
                            }}>

                                <View style={{
                                    padding: 20,
                                    width: "100%",
                                    marginBottom: 20
                                }}>
                                    <Text style={{
                                        fontSize: 20,
                                        marginBottom: 20
                                    }}>
                                        {this.state.question} ?
                    </Text>
                                    <RadioGroup
                                        onSelect={(index, value) => this.onSelect(index, value)}
                                    >
                                        {/* {this.state.polls.map((data, i) => { */}
                                                <RadioButton
                                                    color={"#F44336"}
                                                    value={'a'} >
                                                    <Text>(A) {this.state.polls[0].option}</Text>
                                                </RadioButton>
                                                <RadioButton
                                                    color={"#2196F3"}
                                                    value={'b'}>
                                                    <Text>(B) {this.state.polls[1].option}</Text>
                                                </RadioButton>

                                                <RadioButton
                                                    color={"#FFEB3B"}
                                                    value={'c'}>
                                                    <Text>(C) {this.state.polls[2].option}</Text>
                                                </RadioButton>

                                                <RadioButton
                                                    color={"#4CAF50"}
                                                    value={'d'}>
                                                    <Text>(D) {this.state.polls[3].option}</Text>
                                                </RadioButton>
                                         {/* })} */}
                                    </RadioGroup>
                                </View>
                                <TouchableOpacity
                                    onPress={() => this.submitPoll()}
                                    style={{
                                        backgroundColor: "#FF5721",
                                        width: 120,
                                        height: 40,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 10,
                                        ...Platform.select({
                                            ios: {
                                                shadowColor: 'gray',
                                                shadowOpacity: 0.7,
                                                shadowOffset: { width: 3, height: 3 },
                                                shadowRadius: 1
                                            },
                                            android: { elevation: 2, }
                                        }),
                                        marginBottom: 20
                                    }}>
                                    <Text style={{ color: "white", fontSize: 22 }}>Save</Text>
                                </TouchableOpacity>
                                <PieChart
                                    chart_wh={chart_wh}
                                    series={series}
                                    sliceColor={sliceColor}
                                />
                            </View>
                        </View>
                    </Content>
                }
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#fff',
    },
    title: {
        fontSize: 24,
        margin: 10
    }
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onFetchPoll: () => {
            dispatch(fetchPoll());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Poll);

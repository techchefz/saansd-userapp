//import liraries
import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, SafeAreaView, TextInput, Platform, TouchableOpacity, StatusBar } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Toast } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
import { complaint } from '../redux/reducers/Complaints/action';
import { connect } from 'react-redux';
import config from "../config";
import { AdMobBanner } from "expo";
// create a component
class Complaints extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: this.props.Data.auth.profile.data.name,
            email: this.props.Data.auth.profile.data.email,
            mobileNo: this.props.Data.auth.profile.data.phone,
            complaint: null,
            loader: true,
            userId: null
        }
    }
    onSubmit() {
        const userData = {
            email: this.state.email,
            name: this.state.name,
            mobileNo: this.state.mobileNo,
            complaint: this.state.complaint,
            userId: this.state.userId
        }
        this.props.onComplaint(userData)
    }
    componentWillMount = () => {
      this.setState({
          userId: config.userId
      })
    }
    

    componentWillReceiveProps(nextProp) {
        if (nextProp.Data.complaint.data.success == true) {
            Toast.show({
                text: 'Complaint Sent successfully',
                buttonText: 'Okay'
              })
            this.props.navigation.navigate('Home')
        }
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Complaints</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                <ScrollView
                    style={{ flex: 1, width: '100%' }}
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text style={{ marginTop: 20 }}>Your E-mail address will not br published.</Text>
                    <Text>Requiredfields are marked with *</Text>
                    <View
                        style={{
                            width: "100%",
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20
                        }}>
                        <Text style={styles.title}>Name*</Text>
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Name"
                            style={{ height: 40, borderWidth: 0.5, width: "90%", padding: 10, borderColor: "#afafaf" }}
                            value={this.state.name}
                            onChangeText={(name) => this.setState({ name })}
                        />
                    </View>
                    <View
                        style={{
                            width: "100%",
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20
                        }}>
                        <Text style={styles.title}>Email*</Text>
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Email"
                            value={this.state.email}
                            style={{ height: 40, borderWidth: 0.5, width: "90%", padding: 10, borderColor: "#afafaf" }}
                            onChangeText={(email) => this.setState({ email })}
                        />
                    </View>
                    <View
                        style={{
                            width: "100%",
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20
                        }}>
                        <Text style={styles.title}>Mobile No*</Text>
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Mobile No"
                            value={this.state.mobileNo}
                            style={{ height: 40, borderWidth: 0.5, width: "90%", padding: 10, borderColor: "#afafaf" }}
                            onChangeText={(mobileNo) => this.setState({ mobileNo })}
                        />
                    </View>
                    <View
                        style={{
                            width: "100%",
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 20
                        }}>
                        <Text style={styles.title}>Message / Complaints</Text>
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Write Your Message here....."
                            multiline={true}
                            style={{ minHeight: 150, borderWidth: 0.5, width: "90%", padding: 10, borderColor: "#afafaf" }}
                            onChangeText={(complaint) => this.setState({ complaint })}
                        />
                    </View>
                    <TouchableOpacity style={styles.btn}
                        onPress={() => this.onSubmit()}>
                        <Text style={{ color: "#fff", fontSize: 18 }}>Submit</Text>
                    </TouchableOpacity>
                </ScrollView>
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    title: {
        textAlign: "left",
        width: "90%",
        fontSize: 16,
        marginBottom: 10
    },
    btn: {
        backgroundColor: "#FF5721",
        marginTop: 30,
        marginBottom: 30,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        width: 200,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 1
            },
            android: { elevation: 4 }
        })
    }
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onComplaint: (payload) => {
            dispatch(complaint(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Complaints);

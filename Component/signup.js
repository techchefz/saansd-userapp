//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, Platform, TouchableOpacity, ScrollView, StatusBar, PixelRatio } from 'react-native';
import ZIcon from "react-native-vector-icons/Zocial";
import SIcon from "react-native-vector-icons/SimpleLineIcons";
import EIcon from "react-native-vector-icons/Entypo";
import MIcon from "react-native-vector-icons/MaterialCommunityIcons";
import IIcon from "react-native-vector-icons/Ionicons";
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content } from 'native-base';
import { registerUser } from '../redux/reducers/Register/action';
import { loginUser } from '../redux/reducers/Auth/actions';
import { connect } from 'react-redux';
import config from "../config";

// create a component
class Signup extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: null,
            password: null,
            confirmPassword: null,
            email: null,
            mobileNo: null,
            userId: config.userId
        }
    }
    onRegister() {
        if (this.state.name == null || this.state.password == null || this.state.confirmPassword == null || this.state.email == null || this.state.mobileNo == null) {
            alert("all fields are required")
        } else {
            if (this.state.password == this.state.confirmPassword) {
                const userData = {
                    email: this.state.email,
                    password: this.state.password,
                    name: this.state.name,
                    mobileNo: this.state.mobileNo,
                    userId: this.state.userId
                }
                this.props.onRegisterUser(userData)
            } else {
                alert("Password mismatch")
            }
        }

    }
    componentWillReceiveProps(nextProp) {
        if (nextProp.Data !== null) {
            if (nextProp.Data.success == true) {
                const userData = {
                    email: this.state.email,
                    password: this.state.password,
                    userId: config.userId
                }
                this.props.onLoginUser(userData)
                // this.props.navigation.navigate("MyDrawer")
            }
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.goBack()}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>SignUp</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ width: "100%", flex: 1 }}>
                    <View
                        style={styles.textInput}>
                        <SIcon
                            name="user"
                            size={24}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Name"
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(name) => this.setState({ name })}
                        />
                    </View>
                    <View
                        style={styles.textInput}>
                        <ZIcon
                            name="email"
                            size={24}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Email ID"
                            autoCapitalize="none"
                            keyboardType="email-address"
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(email) => this.setState({ email })}
                        />
                    </View>
                    <View
                        style={styles.textInput}>
                        <EIcon
                            name="mobile"
                            size={24}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Mobile No."
                            maxLength={10}
                            keyboardType="phone-pad"
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(mobileNo) => this.setState({ mobileNo })}
                        />
                    </View>
                    <View
                        style={styles.textInput}>
                        <MIcon
                            name="lock"
                            size={24}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                            secureTextEntry={true}
                            placeholder="Password"
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(password) => this.setState({ password })}
                        />
                    </View>
                    <View
                        style={styles.textInput}>
                        <MIcon
                            name="lock"
                            size={24}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Confirm Password"
                            autoCapitalize="none"
                            secureTextEntry={true}
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                        />
                    </View>
                    <SafeAreaView>
                        <View style={{
                            alignSelf: "center",
                            marginTop:20,
                            justifyContent: "center",
                            height:PixelRatio.getPixelSizeForLayoutSize(50),
                            ...Platform.select({
                                ios: {
                                    shadowColor: '#afafaf',
                                    shadowOpacity: 0.7,
                                    shadowOffset: { width: 3, height: 3 },
                                    shadowRadius: 1
                                },
                                android: { elevation: 2, }
                            })
                        }}>
                            <TouchableOpacity
                                onPress={() => this.onRegister()}
                                style={styles.btn}>
                                <Text style={{ color: "#fff", fontSize: 18 }}>Sign Up</Text>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                </Content>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    textInput: {
        marginTop: 30,
        paddingLeft: 5,
        width: "90%",
        alignSelf: "center",
        flexDirection: "row",
        backgroundColor: "#fff",

        ...Platform.select({
            ios: {
                shadowColor: '#afafaf',
                shadowOpacity: 0.7,
                shadowOffset: { width: 3, height: 3 },
                shadowRadius: 1
            },
            android: { elevation: 2, }
        })
    },
    btn: {
        backgroundColor: "#FF5721",
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 2,
        width: 200,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 0.5
            },
            android: { elevation: 3 }
        })
    }
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onRegisterUser: (payload) => {
            dispatch(registerUser(payload));
        },
        onLoginUser: (payload) => {
            dispatch(loginUser(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        login: state,
        Data: state.register.data,
        userId: state.register.userId
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);

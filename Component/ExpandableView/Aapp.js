import React, { Component } from 'react';
import Style from './Style';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView
} from 'react-native';
import Panel from './Panel';

export default class Aapp extends Component {


  render() {
    return (
      <ScrollView style={Style.list_container}>

        {/* <Text style={{
          fontSize: 23,
          padding: 3,
          textAlign: 'center',
          color: '#009688',
        }}>
          Journey
                </Text> */}

        <Panel title="SOCIAL WORK">
          <View style={{ flex: 1 }}>
            <View style={{ flex: .5, flexDirection: 'column' }} >
              <Text style={Style.list_item}>As the Executive Chairman of the State Planning Board of Maharashtra, Javadekar implemented Government of India’s Watershed Development project in 11 villages in Kolvan Valley, Mulshi Tehsil, Pune District. With perfect CCT (Continuous contour trenching), plantation, embankments and other watershed development activities.  These villages which were cultivating only one crop had later started cultivating three crops, including Sugarcane.  Javadekar was instrumental in raising height of Kolvan Dam and stopping leakages of Hadshi Dam, which benefited the Valley immensely.</Text>
            </View>
          </View>
        </Panel>

        <Panel title="PALDEV EXPERIMENT">
          <View style={{ flex: 1 }}>
            <View style={{ flex: .5, flexDirection: 'column' }} >
              <Text style={Style.list_item}>Under the Pradhan Mantri Adarsh Gram Yojna, Javadekar has adopted the tiniest and remote village Paldev in Bundelkhand, at the border of Uttar Pradesh near Chitrakoot. The village has little over 3000 population, comprising of 25% SCs and 50% Backward Class. The village has no industry and fully depends on monsoon.

{"\n"}Initiatives and success achieved through peoples participation
                
{"\n"}► Substantial improvement in schooling
{"\n"}► Xth Standard results improved from 11% in 2014 to 51% in 2015 and 62% in 2016
{"\n"}► XIIth Standard results improved from 28% in 2014 to 82% in 2015 and stabilised at 78% in 2016.
{"\n"}► Village made Cateract free with cent percent success in Cateract operations of 118 Cateract patients.
{"\n"}► More than 5000 trees planted on the hillock in the village and the survival rate is more than 70% because of peoples participation in watering the plants during summer season.
{"\n"}► More than 2000 fruit-bearing free plants distributed to the farmers and successfully planted and are growing
{"\n"}► 20 youths were skill trained in various disciplines of gainful employment. Sixty women were trained in stitching
{"\n"}► Eight Self-Help Groups formed and are functioning successfully
{"\n"}► Mission ‘Indradhanush’, immunization programme for children, completed</Text>
            </View>
          </View>
        </Panel>

        <Panel title="MINISTER OF STATE">
          <View style={{ flex: 1 }}>
            <View style={{ flex: .5, flexDirection: 'column' }} >
              <Text style={Style.list_item}>
                Key initiatives taken by Javadekar{"\n"}

                As Minister of State for Information & Broadcasting{"\n"}

                ► Initiated process for launching ‘Kisan Channel’{"\n"}
                ► Initiated process for expanding ‘FM’ reach and its second auction{"\n"}
                ► Brought transparency in the allocation of licenses to TV channels{"\n"}
                ► Took various steps to strengthen Social Media outreach of the Government
As Minister of State for Parliamentary Affairs{"\n"}
                {"\n"}
                ► Worked under the leadership of Shri Venkaiah Naidu for the smooth sailing of proceeding in the Rajya Sabha{"\n"}
                ► Actively coordinated with the leaders of all political parties represented in RS
As Minister of State (IC) for Environment, Forest and Climate Change{"\n"}
                {"\n"}
                ► Took various initiatives to increase forest cover. Introduced CAMPA Bill in Parliament to release Rs 42,000 crore for afforestation{"\n"}
                ► Ensured transparency in the process of granting Environmental Approvals and introduced the concept of “Ease of Doing Responsible Business”{"\n"}
                ► Effectively put up India’s case in Paris and played a pro-active role in the success of Paris Summit on Climate Change{"\n"}
                ► Revamped Five Waste Management Rules – Solid, Plastic, Hazardous, Electronic and Bio-medical. Brought in for the first time Construction and Demolition Waste Management Rules{"\n"}
                ► Introduced comprehensive National Air Quality Index and raised pollution benchmarks for industries and also started 24×7 pollution monitoring mechanism{"\n"}
                ► More than 400, Eco-Sensitive Zones finalised{"\n"}
              </Text>
            </View>
          </View>
        </Panel>

        <Panel title="UNION MINISTER FOR HUMAN RESOURCE DEVELOPMENT (HRD)">
          <View style={{ flex: 1 }}>
            <View style={{ flex: .5, flexDirection: 'column' }} >
              <Text style={Style.list_item}>
                Prime Minister Narendra Modi effected a Cabinet reshuffle on July 5, 2016. He inducted 19 new Ministers of State while changing portfolios and several other Ministers.  Prakash Javadekar was given the responsibility of the HRD ministry. Considering, the ministry is responsible for shaping the future of India’s young generation, which is a huge responsibility.
{"\n"}
                {"\n"}VISION:  His vision for the Ministry of Human Resource Development is very simple and can be categorized into 4 major divisions:
{"\n"}
                {"\n"}ACCESSABLITY- ‘सबको शिक्षा’
{"\n"}
                {"\n"}QUALITY- ‘अच्छी शिक्षा ’
{"\n"}
                {"\n"}AFFORDABLITY- Lack of money should not be a hindrance in anyone’s education.
{"\n"}
                {"\n"}ACCOUNTABLITY- All teachers, institutes and everybody involved in education has accountability as they can shape the future of India.
{"\n"}
                {"\n"}EQUITY- means that personal or social circumstances does not become an obstacle in achieving educational potential and that all individuals reach to their potential. This is social justice.
                 </Text>
            </View>
          </View>
        </Panel>

      </ScrollView>
    );
  }
}


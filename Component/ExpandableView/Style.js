
import { StyleSheet ,Dimensions} from 'react-native';
const { width, height } = Dimensions.get('window');
const gutter = 3; // You can add gutter if you want

var Style = StyleSheet.create({
  list_container: {
    flex: 1,
    width:'100%',
    height: "100%",
    flexDirection: 'column',
    borderRadius: 4,
    margin:3,
    padding:10,
    backgroundColor : 'transparent',
    paddingTop : 30,
    borderWidth: 2,
    borderColor: 'transparent'
  },
  list_item: {
    fontSize: 15,
  },
  list_header: {
    fontSize: 17,
    margin:10,
    flex    : 1,
    fontWeight :'bold',
    color:'#2196F3',
  },
  list_sub_header: {
    fontSize: 17,
    fontWeight :'bold',
  },

});

export default Style;

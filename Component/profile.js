//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Platform, Image, ScrollView, PixelRatio } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content, Spinner } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
import { fetchProfileData } from '../redux/reducers/Profile/actions';
import { connect } from 'react-redux';
import { AdMobBanner } from "expo";
// create a component
class Profile extends Component {
    constructor(props) {
        super(props)

        this.state = {
            profileData: null,
            loader: true
        }
    }

    componentWillMount() {
        this.props.onUserProfile()
        if (this.state.profileData == null) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.Data.profile.data.data.length !== 0) {
            this.setState({
                loader: false,
                profileData: nextProps.Data.profile
            })
        }
    }


    render() {
        // console.log('====================================')
        // console.log(this.state.profileData.data.data[0].profilePicture)
        // console.log('====================================')
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Profile</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                {this.state.loader == true ?
                    <Spinner />
                    :
                    <Content style={{ padding: 20, marginBottom: 10, }}
                        contentContainerStyle={{ justifyContent: "center", alignItems: "center", width: "100%" }}>
                        <View style={styles.card}>
                            <View style={{ flex: 1.2, padding: 10 }}>
                                <Image
                                    source={{ uri: this.state.profileData.data.data[0].profilePicture }}
                                    style={{
                                        height: PixelRatio.getPixelSizeForLayoutSize(80),
                                        width: "100%",
                                        borderRadius: 10
                                    }} />
                            </View>
                            <View style={{ flex: 2, padding: 10 }}>
                                <Text style={{ fontWeight: "bold", fontSize: 16 }}>Name: </Text>
                                <Text>{this.state.profileData.data.data[0].name}{'\n'}</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 16 }}>Date of Birth: </Text>
                                <Text>{this.state.profileData.data.data[0].dateOfBirth}{'\n'}</Text>
                                <Text style={{ fontWeight: "bold", fontSize: 16 }}>Edu Qualification: </Text>
                                <Text>{this.state.profileData.data.data[0].qualification}{'\n'}</Text>
                                {/* <Text style={{ fontWeight: "bold", fontSize: 16 }}>Profession: </Text>
                            <Text>Politician{'\n'}</Text> */}
                            </View>
                        </View>
                        <View style={{ width: "100%" }}>
                            <Text style={{ fontWeight: "bold", fontSize: 16 }}>{this.state.profileData.data.data[0].about}</Text>
                        </View>
                        <View style={{ width: "100%", height: 50 }} />
                    </Content>
                }
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#fff',
    },
    card: {
        marginTop: 20,
        marginBottom: 10,
        backgroundColor: "#fff",
        borderRadius: 10,
        ...Platform.select({
            ios: {
                shadowColor: '#afafaf',
                shadowOpacity: 0.7,
                shadowOffset: { width: 3, height: 3 },
                shadowRadius: 1
            },
            android: { elevation: 2, }
        }),
        width: "100%",
        // height: 180,
        flexDirection: "row"
    }
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onUserProfile: () => {
            dispatch(fetchProfileData());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

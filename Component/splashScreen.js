//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

// create a component
class SplashScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    source={require("../assets/bjp.png")}
                    style={{ width: 150, height: 150, resizeMode: "contain" }} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#FF5721',
    },
});

//make this component available to the app
export default SplashScreen;

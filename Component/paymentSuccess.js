//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Spinner, Content } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
// create a component
class PaymentSuccess extends Component {
    constructor(props) {
        super(props)

        this.state = {
            loader: true
        }
    }

    componentWillMount = () => {
        setTimeout(() => { this.setState({ loader: false }),
        setTimeout(() => { this.props.navigation.navigate('Home') }, 5000)
     }, 5000)
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.loader == true ?
                    <View style={{
                        flex: 1,
                        width: "100%",
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#fff',
                    }}>
                        <Spinner />
                        <Text style={{
                            width: "70%",
                            textAlign: "center",
                            fontSize: 18,
                            color: "#666666"
                        }}>Please wait your payment is in progress. Do not close or refresh the app</Text>
                    </View> :
                    <View style={{
                        flex: 1,
                        width: "100%",
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#fff',
                    }}>
                        <Header
                            androidStatusBarColor='#E63900'
                            iosBarStyle="light-content"
                            style={{
                                width: "100%",
                                backgroundColor: "#FF5721",
                                elevation: 8
                            }}>
                            <Left style={{ paddingLeft: 10, flex: 1 }}/>
                            <Body style={{ flex: 2 }}>
                                <Title style={{ color: "#fff" }}>Payment Success</Title>
                            </Body>
                            <Right style={{ flex: 1 }} />
                        </Header>
                        <View style={{flex:1, width:"100%", alignItems:"center", paddingTop:100}}>
                            <View style={{ width:"100%"}}>
                                <Image source={require('../assets/success.png')}
                                    style={{ width: "100%", height: 200, resizeMode: "contain" }} />
                                    
                                    <Spinner/>
                            </View>
                        </View>
                    </View>
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default PaymentSuccess;

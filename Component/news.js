//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, WebView, Image, TouchableOpacity, Linking, Platform } from 'react-native';
import IIcon from "react-native-vector-icons/Ionicons";
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content } from 'native-base';
import { socialLinkData } from "../redux/reducers/socialLinks/actions";
import { connect } from 'react-redux';

// create a component
class News extends Component {
    constructor(props) {
        super(props);

        this.state = {
            videoInfo: [],
            FaceBookInfo: [],
            youtubeChannelId: 'UC_5HPOWK_SDqV5FszV7HS7A'
        };
        this.VideoList = this.VideoList.bind(this);
    }


    VideoList() {
        fetch(`https://www.googleapis.com/youtube/v3/search?key=AIzaSyAp-3SyOKF7ZKP8hK7pThCCbkaQLHJA40w&channelId=${this.state.youtubeChannelId}&part=snippet,id&order=date&maxResults=5`)
            .then(resp => resp.json())
            .then((resp) => {
                //this.setState({video: resp.results});
                this.setState({ videoInfo: resp.items });
            });
    }
    FaceBookList() {
        fetch('https://graph.facebook.com/narendramodi/posts?fields=full_picture,picture,link,message,created_time&limit=5&access_token=EAAen3bl1iDgBABqdvklyfmkXy9vZBaI5CNlKoOQzByH9fdVlXHO1rg0W7TQeD9YZAeWGxcF4b7Wti0ZB2y9ArRuSmTcp7QqMAxZAYD7HVtXZAeeHhvYjQBsEgPud46HqpXjZCEbimZC9ZCaiAYNHa1Ap23A7MfxE6KkZD')
            .then(resp => resp.json())
            .then((resp) => {
                // console.log('=============resp=======================');
                // console.log(resp);
                // console.log('=============resp=======================');
                this.setState({ FaceBookInfo: resp.data });
            });
    }
    componentWillMount() {
        this.VideoList();
        this.FaceBookList();
        this.props.onsocialLinkData();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.Data.socialLinks.data !== null) {
            nextProps.Data.socialLinks.data.data.forEach(dataLinks => {
                this.setState({
                    youtubeChannelId: dataLinks.youtubeLink
                })
            })
            this.VideoList();
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10, flex: 1 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body style={{ flex: 2 }}>
                        <Title style={{ color: "#fff" }}>News</Title>
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                <Content style={{ padding: 10, width: "100%" }}>
                    {this.state.videoInfo.map((video, i) => {
                        return (
                            this.state.videoInfo == undefined ? null : (
                                <TouchableOpacity
                                    onPress={() => Linking.openURL('https://www.youtube.com/watch?v=' + video.id.videoId)}
                                    key={i}
                                    style={{
                                        width: "100%",
                                        height: 100,
                                        borderRadius: 2,
                                        padding: 5,
                                        borderWidth: 0.5,
                                        borderColor: "#e4e4e4",
                                        marginBottom: 10,
                                        flexDirection: "row",
                                        elevation: 1
                                    }}>
                                    <View style={{ flex: 2, justifyContent: "center", alignItems: "center", padding: 1 }}>
                                        <Image source={{ uri: video.snippet.thumbnails.medium.url }}
                                            style={{ height: "100%", width: "100%" }} />
                                        <Image
                                            source={require('../assets/ytLogo.png')}
                                            style={{ height: 30, width: 30, resizeMode: "contain", position: "absolute", top: 0, left: 0 }} />
                                    </View>
                                    <View style={{ flex: 6, padding: 2 }}>
                                        <Text numberOfLines={2} style={{ fontWeight: "bold" }}>{video.snippet.title} </Text>
                                        <Text
                                            numberOfLines={3}
                                            style={{ fontSize: 12, }}>Description : <Text
                                                style={{ fontSize: 10, paddingLeft: 1, textAlign: "left" }}> {video.snippet.description} </Text></Text>

                                    </View>

                                </TouchableOpacity>
                            )

                        )

                    })}
                    {/* {this.state.FaceBookInfo.map((video, j) => {
                        return (
                            <TouchableOpacity
                                onPress={() => Linking.openURL(video.link)}
                                key={j}
                                style={{
                                    width: "100%",
                                    height: 100,
                                    borderRadius: 2,
                                    padding: 5,
                                    borderWidth: 0.5,
                                    borderColor: "#e4e4e4",
                                    marginBottom: 10,
                                    flexDirection: "row",
                                    elevation: 1
                                }}>
                                <View style={{ flex: 2, justifyContent: "center", alignItems: "center", padding: 1 }}>
                                    <Image source={{ uri: video.full_picture }}
                                        style={{ height: "100%", width: "100%" }} />
                                    <Image
                                        source={require('../assets/fbLogo.png')}
                                        style={{ height: 30, width: 30, resizeMode: "contain", position: "absolute", top: 0, left: 0 }} />
                                </View>
                                <View style={{ flex: 6, padding: 2 }}>
                                    <Text
                                        numberOfLines={5}
                                        style={{ fontSize: 12, fontWeight: "500" }}>{video.message}</Text>

                                </View>
                            </TouchableOpacity>
                        )
                    })} */}
                </Content>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onsocialLinkData: () => {
            dispatch(socialLinkData());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(News);

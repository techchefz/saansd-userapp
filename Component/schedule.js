//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, Dimensions, ScrollView, Image, Platform } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Spinner } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
import { meeting } from '../redux/reducers/Meeting/action';
import { connect } from 'react-redux';
import moment from "moment";
import { AdMobBanner } from "expo";
// create a component
class Schedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Meeting: [],
            loader: true
        }
    }

    componentWillMount() {
        this.props.onMeeting()
        if (this.state.Meeting.length == 0) {
            this.setState({
                loader: true
            })
        } else {
            this.setState({
                loader: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.Data.meeting.data.data.length !== 0) {
            this.setState({
                loader: false,
                Meeting: nextProps.Data.meeting.data.data
            })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Schedule</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                {this.state.loader == true ? 
                <View
                style={{
                    flex: 1,
                    width: "100%",
                    alignItems: 'center',
                }}>
                <ScrollView style={{ width: "100%", flex: 1 }}
                    contentContainerStyle={{ justifyContent: "center", alignItems: 'center' }}>
                        <View style={styles.card}>
                                <Text style={styles.cardTitle}>No Meeting Scheduled</Text>
                        </View>
                    <View style={{width:"100%", height:30}}/>
                </ScrollView>
            </View>
                : 
                <View
                    style={{
                        flex: 1,
                        width: "100%",
                        alignItems: 'center',
                    }}>
                    <ScrollView style={{ width: "100%", flex: 1 }}
                        contentContainerStyle={{ justifyContent: "center", alignItems: 'center' }}>
                        {this.state.Meeting.map((data, i) =>
                            <View key={i} style={styles.card}>
                                <View style={{ flex: 1, borderRadius: 5, padding: 10 }}>
                                    <Image
                                        source={require('../assets/iconBjp.png')}
                                        style={{
                                            maxHeight: 100,
                                            width: "100%",
                                            borderRadius: 5,
                                            resizeMode:"stretch"
                                        }} />
                                </View>
                                <View style={{ flex: 2.5, padding: 10 }}>
                                    <Text style={styles.cardTitle}>Date : {moment(data.venueDateTime).format("MMM Do YY")}</Text>
                                    <Text style={styles.cardTitle}>Time: {moment(data.venueDateTime).format("hh:mm:ss a")}</Text>
                                    <Text style={styles.cardTitle}>Venue: {data.venueName}</Text>
                                </View>
                            </View>
                        )}
                        <View style={{width:"100%", height:30}}/>
                    </ScrollView>
                </View>
                }
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-7271832886181075/7356669820"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    card: {
        borderRadius: 5,
        marginTop: 20,
        backgroundColor: "#fff",
        ...Platform.select({
            ios: {
                shadowColor: '#afafaf',
                shadowOpacity: 0.7,
                shadowOffset: { width: 3, height: 3 },
                shadowRadius: 5
            },
            android: { elevation: 2, }
        }),
        width: "90%",
        padding: 10,
        flexDirection: "row",
        // minHeight: 130,
        maxHeight: 150
    },
    cardTitle: {
        fontSize: 18,
        marginBottom: 10
    }
});

//make this component available to the app
function mapDispatchToProps(dispatch) {
    return {
        onMeeting: () => {
            dispatch(meeting());
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);

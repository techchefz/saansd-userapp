//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, Platform, TouchableOpacity, ScrollView, StatusBar, AsyncStorage } from 'react-native';
import ZIcon from "react-native-vector-icons/Zocial";
import EIcon from "react-native-vector-icons/Ionicons";
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { loginUser } from '../redux/reducers/Auth/actions';
import { connect } from 'react-redux';
import config from "../config";

// create a component
class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: null,
            password: null,
            emailValidate: null,
        }
    }
    onSubmit() {
        if (this.state.email == null && this.state.password == null) {
            Toast.show({
                text: 'All fields are required',
              })
        } else {
            const userData = {
                email: this.state.email,
                password: this.state.password,
                userId: config.userId
            }
            this.props.onLoginUser(userData)
        }
    }
    componentWillMount() {
        this.setState({
            email: null,
            password: null
        })
        this.getToken()
    }
    getToken = async () => {
        try {
            let user = await AsyncStorage.getItem('user');
            let parsed = JSON.parse(user);
            let Token = parsed.accessToken
            let email = parsed.email
            let password = parsed.password
            if (Token) {
                // this.props.navigation.navigate("MyDrawer")
                this.setState({
                    email: email,
                    password: password
                })
                const userData = {
                    email: this.state.email,
                    password: this.state.password,
                    userId: config.userId
                }
                this.props.onLoginUser(userData)
            }
        } catch (error) {
            console.log(error);
        }
    }
    componentWillReceiveProps(nextProp) {
        if (nextProp.Data.auth.profile !== null) {
            if (nextProp.Data.auth.profile.success == true) {
                if (nextProp.Data.auth.profile.jwtAccessToken !== null) {
                    let obj = {
                        accessToken: nextProp.Data.auth.profile.jwtAccessToken,
                        email: this.state.email,
                        password: this.state.password
                    }
                    AsyncStorage.setItem('user', JSON.stringify(obj));
                    this.props.navigation.navigate("MyDrawer");
                }
            }
        }
    }
    validate(text, type) {
        alph = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        if (type == 'email') {
            if (alph.test(text)) {
                this.setState({
                    emailValidate: true,
                    email: text
                })
            } else {
                this.setState({
                    emailValidate: false
                })
            }
        }
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <View style={{ height: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight }} /> */}
                {/* <SafeAreaView
                    style={{ backgroundColor: "#FF5721", width: "100%", }} > */}

                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left />
                    <Body>
                        <Title style={{ color: "#fff" }}>Login</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                <ScrollView
                    contentContainerStyle={{
                        justifyContent: "center",
                        flex: 1
                    }}
                    showsVerticalScrollIndicator={false}>
                    <View
                        style={[styles.textInput, { shadowColor: this.state.emailValidate == null ? '#afafaf' : this.state.emailValidate == true ? 'green' : 'red' }]}>
                        <ZIcon
                            name="email"
                            size={24}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Email ID"
                            autoCapitalize="none"
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(text) => this.validate(text, 'email')}
                        />
                    </View>
                    <View
                        style={styles.textInput}>
                        <EIcon
                            name="md-lock"
                            size={30}
                            style={{ flex: 1, alignSelf: "center", color: "gray" }} />
                        <TextInput
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                            secureTextEntry={true}
                            placeholder="Password"
                            style={{ flex: 9, height: 40 }}
                            onChangeText={(password) => this.setState({ password })}
                        />
                    </View>
                    {/* <View style={{
                        alignSelf: "flex-end",
                        padding: 15
                    }}>
                        <Text style={{ color: "#FF5721", fontSize: 14 }}>Forgot Password ?</Text>
                    </View> */}
                    <View style={{
                        alignSelf: "center",
                    }}>
                        <TouchableOpacity
                            onPress={() => this.onSubmit()}
                            // onPress={() => this.props.navigation.navigate('MyDrawer')}
                            style={styles.btn}>
                            <Text style={{ color: "#fff", fontSize: 18 }}>Login</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        alignSelf: "center",
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: 20
                    }}>
                        <View style={{
                            borderTopWidth: 0.5,
                            borderColor: "gray",
                            width: "40%"
                        }} />
                        <View style={{
                            backgroundColor: "#FF5721",
                            height: 40,
                            width: 40,
                            borderRadius: 20,
                            justifyContent: "center",
                            alignItems: "center",
                        }}>
                            <Text
                                style={{
                                    backgroundColor: "transparent",
                                    color: "white"
                                }}>OR</Text>
                        </View>
                        <View style={{
                            borderTopWidth: 0.5,
                            borderColor: "gray",
                            width: "40%"
                        }} />
                    </View>
                    {/* <View style={{
                        alignSelf: "center",
                    }}>
                        <TouchableOpacity style={[styles.btn, { backgroundColor: "#3b5998", width: 300 }]}>
                            <Text style={{ color: "#fff", fontSize: 18 }}>Facebook</Text>
                        </TouchableOpacity>
                    </View> */}
                    <View style={{
                        alignSelf: "center",
                    }}>
                        <TouchableOpacity
                            style={[styles.btn, { backgroundColor: "#afafaf", width: 300, marginTop: 80 }]}>
                            <Text style={{ color: "#fff", fontSize: 18 }}>Continue as Guest</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        alignSelf: "center",
                    }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('SignUp')}
                            style={[styles.btn, { width: 300, marginTop: 10, marginBottom: 20 }]}>
                            <Text style={{ color: "#fff", fontSize: 18 }}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        width: "100%",
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    textInput: {
        marginTop: 30,
        paddingLeft: 5,
        width: "90%",
        alignSelf: "center",
        flexDirection: "row",
        backgroundColor: "#fff",

        ...Platform.select({
            ios: {
                shadowColor: '#afafaf',
                shadowOpacity: 0.7,
                shadowOffset: { width: 3, height: 3 },
                shadowRadius: 1
            },
            android: { elevation: 2, }
        })
    },
    btn: {
        backgroundColor: "#FF5721",
        marginTop: 30,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 2,
        width: 200,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: {
                    width: 2,
                    height: 2
                },
                shadowRadius: 2,
                shadowOpacity: 0.5
            },
            android: { elevation: 3 }
        })
    }
});

//make this component available to the app

function mapDispatchToProps(dispatch) {
    return {
        onLoginUser: (payload) => {
            dispatch(loginUser(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        Data: state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

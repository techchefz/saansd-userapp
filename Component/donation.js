//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Platform, TouchableOpacity, ImageBackground, TouchableHighlight } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
// import RazorpayCheckout from 'react-native-razorpay';

// create a component
class Donation extends Component {
    render() {
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.navigate('Home')}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Donation</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                <ImageBackground
                    source={require("../assets/bjp_bg.png")}
                    style={{
                        width: "100%",
                        flex: 1,
                    }}>
                    <ScrollView
                        style={{ width: "100%", flex: 1 }}
                        contentContainerStyle={{
                            justifyContent: "space-around",
                            alignItems: 'center',
                            flexDirection: "row",
                            flexWrap: "wrap",
                        }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>₹ 100</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>₹ 200</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>₹ 500</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>₹ 1000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>₹ 2000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>₹ 5000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('PaymentPage')}
                            style={styles.card}>
                            <Text style={{ fontSize: 28 }}>Other</Text>
                        </TouchableOpacity>
                        <TouchableHighlight onPress={() => {
                            var options = {
                                description: 'Credits towards consultation',
                                image: 'https://i.imgur.com/3g7nmJC.png',
                                currency: 'INR',
                                key: 'rzp_test_1DP5mmOlF5G5ag',
                                amount: '5000',
                                name: 'foo',
                                prefill: {
                                    email: 'void@razorpay.com',
                                    contact: '9191919191',
                                    name: 'Razorpay Software'
                                },
                                theme: { color: '#F37254' }
                            }
                            RazorpayCheckout.open(options).then((data) => {
                                // handle success
                                alert(`Success: ${data.razorpay_payment_id}`);
                            }).catch((error) => {
                                // handle failure
                                alert(`Error: ${error.code} | ${error.description}`);
                            });
                        }}/>
                    </ScrollView>
                </ImageBackground>
            </View>
                );
            }
        }
        
        // define your styles
const styles = StyleSheet.create({
                    container: {
                    flex: 1,
                // justifyContent: 'center',
                width: "100%",
                alignItems: 'center',
                backgroundColor: '#fff',
            },
    card: {
                    ...Platform.select({
                        ios: {
                            shadowColor: '#afafaf',
                            shadowOpacity: 0.7,
                            shadowOffset: { width: 3, height: 3 },
                            shadowRadius: 5
                        },
                        android: { elevation: 2, }
                    }),
                backgroundColor: "#ffddcc",
                width: 140,
                height: 70,
                borderRadius: 5,
                justifyContent: "center",
                alignItems: "center",
                marginBottom: 20,
                marginTop: 20
            }
        });
        
        //make this component available to the app
        export default Donation;

//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title, Content, Form, Item, Input, Label } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";
// create a component
class NetBanking extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10, flex: 1 }}>
                        <IIcon
                            onPress={() => this.props.navigation.goBack()}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body style={{ flex: 2 }}>
                        <Title style={{ color: "#fff" }}>Net Banking</Title>
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                <Content style={{ flex: 1, width: "100%", backgroundColor: "#e4e4e4" }}
                    contentContainerStyle={{ justifyContent: "center", alignItems: "center", padding: 10 }}>
                    <Text style={{ fontSize: 22, color: "#666666", margin: 10 }}>Welcome to Internet Banking</Text>
                    <View style={{
                        backgroundColor: "white",
                        width: "90%",
                        justifyContent:"center",
                        alignItems:"center",
                        ...Platform.select({
                            ios: {
                                shadowColor: '#000',
                                shadowOffset: {
                                    width: 2,
                                    height: 2
                                },
                                shadowRadius: 2,
                                shadowOpacity: 1
                            },
                            android: { elevation: 4 }
                        })
                    }}>
                        <Text style={{fontSize: 22, color: "#666666", margin: 10, marginTop:10 }}>Sign In</Text>
                        <Form style={{width:"100%", padding:10}}>
                            <Item floatingLabel>
                                <Label>Email </Label>
                                <Input keyboardType="email-address"/>
                            </Item>
                            <Item floatingLabel >
                                <Label>Password</Label>
                                <Input secureTextEntry={true}/>
                            </Item>
                        </Form>
                        <Text style={{width:"80%", textAlign:"center"}}>By signing in you accept the 
                            <Text style={{
                                color:"blue",
                                textDecorationLine:"underline"
                            }}> terms and conditions</Text></Text>
                        <Button style={{alignSelf:"center", width:150, marginTop:20, marginBottom:30, justifyContent:"center", alignItems:"center"}} success>
                        <Text style={{color:"white", fontWeight:"bold"}}> Sign In </Text>
                        </Button>
                    </View>
                </Content>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default NetBanking;

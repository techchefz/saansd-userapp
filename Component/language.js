//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import { Container, Header, Left, Body, Right, Button, Title } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";

// create a component
class Language extends Component {
    render() {
        return (
            <View style={styles.container}>
                {/* <SafeAreaView style={{ backgroundColor: "#FF5721", width: "100%", }} > */}
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10 }}>
                        <IIcon
                            onPress={() => this.props.navigation.goBack()}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body>
                        <Title style={{ color: "#fff" }}>Language</Title>
                    </Body>
                    <Right />
                </Header>
                {/* </SafeAreaView> */}
                <View style={{
                    padding: 20,
                    borderBottomWidth: 3,
                    borderColor: "#afafaf"
                }}>
                    <Text style={{ fontSize: 18 }}>
                        You can change this section anytime by going to the {
                            <Icon
                                name="md-settings"
                                size={18} />
                        } settings selection
                   </Text>
                </View>
                <View style={{
                    width: "100%",
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <View style={styles.listStyle}>
                        <Text style={{ fontSize: 18 }}>English</Text>
                    </View>
                    <View style={styles.listStyle}>
                        <Text style={{ fontSize: 18 }}>हिन्दी</Text>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        backgroundColor: '#fff',
    },
    listStyle: {
        width: "90%",
        borderBottomWidth: 1,
        margin: 10,
        padding: 10,
        borderColor: "gray"
    }
});

//make this component available to the app
export default Language;

//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import { Container, Header, Left, Body, Right, Button, Icon, Title, Content } from 'native-base';
import IIcon from "react-native-vector-icons/Ionicons";

// create a component
class MyClass extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    androidStatusBarColor='#E63900'
                    iosBarStyle="light-content"
                    style={{
                        width: "100%",
                        backgroundColor: "#FF5721",
                        elevation: 8
                    }}>
                    <Left style={{ paddingLeft: 10, flex: 1 }}>
                        <IIcon
                            onPress={() => this.props.navigation.goBack()}
                            name="md-arrow-round-back"
                            size={35}
                            color="#fff" />
                    </Left>
                    <Body style={{ flex: 2 }}>
                        <Title style={{ color: "#fff" }}>Card Payment</Title>
                    </Body>
                    <Right style={{ flex: 1 }} />
                </Header>
                <Content style={{ paddingTop: 20 }}>
                    <CreditCardInput
                        autoFocus
                        requiresName
                        requiresCVC
                        validColor={"black"}
                        invalidColor={"red"}
                        onChange={this._onChange} />
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                        <Button
                            style={{ alignSelf: "center", justifyContent: "center", marginTop: 20, flex: 1, width: 100 }}
                          onPress={()=> this.props.navigation.navigate('paymentSuccess')}
                        >
                            <Text style={{ color: "white" }}>Pay Now</Text>
                        </Button>
                    </View>
                </Content>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default MyClass;

import config from "../../../config";
import axios from "axios";
import { MEETING_DATA, MEETING_DATA_SUCCESS, MEETING_DATA_FAILED } from "./actionType";

export function meetingSuccess(payload) {
    return {
        type: MEETING_DATA_SUCCESS,
        payload
    }
}


export function meetingFailed(payload) {
    return {
        type: MEETING_DATA_FAILED,
        payload
    }
}

export function meetingData(payload) {
    return {
        type: MEETING_DATA,
        payload
    }
}

export const meeting = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/fetchMeeting`,
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } })
            .then((res) => {
                if (res.data.success === true) {
                    dispatch(meetingSuccess(res.data))
                }
            }).catch((err) => {
                console.log(err);
            });
};
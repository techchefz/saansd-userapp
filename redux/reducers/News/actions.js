import config from "../../../config";
import axios from "axios";
import { JOURNEY_DATA_FETCHED, JOURNEY_DATA_FETCH_FAILED } from "./actionType";

export function fetchJourney(payload) {
    return {
        type: JOURNEY_DATA_FETCHED,
        payload
    }
}


export function fetchJourneyFailed(payload) {
    return {
        type: JOURNEY_DATA_FETCH_FAILED,
        payload
    }
}


export const fetchJourneyData = () => {
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getJourney`).then((res) => {
            if (res.data.success === true) {
                dispatch(fetchJourney(res.data.data[0]))
            }
        }).catch((err) => {
            console.log(err);
        });
};
import { REGISTER_USER_SUCCESS } from "./actionType";

const initialState = {
    data: null,
    token: null,
    userId: "5b87dea69caa497737cdc668"
}

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                data: action.payload,
                token: action.payload.jwtAccessToken,
            };
        default:
            return state;
    }
}
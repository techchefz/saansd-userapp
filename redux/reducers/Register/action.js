import config from "../../../config";
import axios from "axios";
import { REGISTER_USER, REGISTER_USER_SUCCESS, REGISTER_USER_FAILED } from "./actionType";

export function registerSuccess(payload) {
    return {
        type: REGISTER_USER_SUCCESS,
        payload
    }
}


export function registerFailed(payload) {
    return {
        type: REGISTER_USER_FAILED,
        payload
    }
}

export function registeringUser(payload) {
    return {
        type: REGISTER_USER,
        payload
    }
}

export const registerUser = userCredentials => {
    return (dispatch, getState) =>
        axios.post(`${config.serverUrl}:${config.port}/node/api/admin/register`, userCredentials).then((res) => {
            if (res.data.success === true) {
                dispatch(registerSuccess(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};
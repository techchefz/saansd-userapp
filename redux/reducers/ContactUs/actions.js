import config from "../../../config";
import axios from "axios";
import { CONTACT_DATA_FETCHED, CONTACT_DATA_FETCH_FAILED } from "./actionType";

export function fetchContact(payload) {
    return {
        type: CONTACT_DATA_FETCHED,
        payload
    }
}


export function fetchContactFailed(payload) {
    return {
        type: CONTACT_DATA_FETCH_FAILED,
        payload
    }
}


export const fetchContactData = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getContact`,
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
            if (res.data.success === true) {
                dispatch(fetchContact(res.data.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};
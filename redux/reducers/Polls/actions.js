import config from "../../../config";
import axios from "axios";
import { POLL_DATA_FETCHED, POLL_DATA_FETCH_FAILED } from "./actionType";

export function fetchPollData(payload) {
    return {
        type: POLL_DATA_FETCHED,
        payload
    }
}


export function fetchPollDataFailed(payload) {
    return {
        type: POLL_DATA_FETCH_FAILED,
        payload
    }
}


export const fetchPoll = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/fetchPoll`,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
            if (res.data.success === true) {
                dispatch(fetchPollData(res.data.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};
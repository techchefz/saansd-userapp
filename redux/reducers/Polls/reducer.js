import { POLL_DATA_FETCHED } from "./actionType";

const initialState = {
    data: [],
}

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case POLL_DATA_FETCHED:
            return {
                ...state,
                data: action.payload,
            };
        default:
            return state;
    }
}
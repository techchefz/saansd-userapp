import config from "../../../config";
import axios from "axios";
import { COMPLAINT_DATA, COMPLAINT_DATA_SUCCESS, COMPLAINT_DATA_FAILED } from "./actionType";

export function complaintSuccess(payload) {
    return {
        type: COMPLAINT_DATA_SUCCESS,
        payload
    }
}


export function complaintFailed(payload) {
    return {
        type: COMPLAINT_DATA_FAILED,
        payload
    }
}

export function complaintData(payload) {
    return {
        type: COMPLAINT_DATA,
        payload
    }
}

export const complaint = userCredentials => {
    return (dispatch, getState) =>
        axios.post(`${config.serverUrl}:${config.port}/node/api/admin/complaints`, userCredentials ).then((res) => {
            if (res.data.success === true) {
                dispatch(complaintSuccess(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};
import { COMPLAINT_DATA_SUCCESS } from "./actionType";

const initialState = {
    data: null,
}

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case COMPLAINT_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload,
            };
        default:
            return state;
    }
}
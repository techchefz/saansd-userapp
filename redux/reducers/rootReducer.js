import { combineReducers } from "redux";
import auth from "./Auth/reducer";
import register from "./Register/reducer";
import profile from "./Profile/reducer";
import contactUs from "./ContactUs/reducer";
import journey from "./Journey/reducer";
import meeting from "./Meeting/reducer";
import complaint from "./Complaints/reducer";
import news from "./News/reducer";
import polls from "./Polls/reducer";
import featuredImage from "./FeaturedImage/reducer";
import socialLinks from "./socialLinks/reducer";
import achievement from "./Achievement/reducer";

export default combineReducers({
    auth,
    register,
    profile,
    contactUs,
    journey,
    meeting,
    complaint,
    news,
    polls,
    featuredImage,
    socialLinks,
    achievement
 });
 
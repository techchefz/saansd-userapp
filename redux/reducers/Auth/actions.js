import config from '../../../config';
import axios from 'axios';
import { LOGIN_USER_SUCCESS, LOGIN_USER_FAILED, LOGIN_USER } from './actionType';
import { Toast } from "native-base";

export function loginSuccess(payload) {
    return {
        type: LOGIN_USER_SUCCESS,
        payload
    }
}


export function loginFailed(payload) {
    return {
        type: LOGIN_USER_FAILED,
        payload
    }
}

export function loggingInUser(payload) {
    return {
        type: LOGIN_USER,
        payload
    }
}

export const loginUser = userCredentials => {
    return (dispatch, getState) =>
        axios.post(`${config.serverUrl}:${config.port}/node/api/auth/login`, userCredentials).then((res) => {
            if (res.data.success === true) {
                dispatch(loginSuccess(res.data))
            } else if(res.data.success === false){
                Toast.show({
                    text: 'Invalid User',
                  })
            }
        }).catch((err) => {
            console.log(err);
        });
};
import { LOGIN_USER_SUCCESS } from './actionType'

const initialState = {
    profile: null,
    token: null,
    userId: "5b87dea69caa497737cdc668"
};

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                profile: action.payload, 
                token: action.payload.jwtAccessToken

            };
        default:
            return state;
    }
}
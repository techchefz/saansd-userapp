import config from "../../../config";
import axios from "axios";
import { ACHIEVEMENT_DATA_FETCHED, ACHIEVEMENT_DATA_FETCH_FAILED } from "./actionType";

export function fetchAchievement(payload) {
    return {
        type: ACHIEVEMENT_DATA_FETCHED,
        payload
    }
}


export function fetchAchievementFailed(payload) {
    return {
        type: ACHIEVEMENT_DATA_FETCH_FAILED,
        payload
    }
}


export const fetchAchievementData = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getAchievement`,
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
            if (res.data.success === true) {
                dispatch(fetchAchievement(res.data.data[0]))
            }
        }).catch((err) => {
            console.log(err);
        });
};
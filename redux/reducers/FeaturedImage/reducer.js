import { FEATURED_IMAGE_FETCHED } from "./actionType";

const initialState = {
    data: null,
}

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case FEATURED_IMAGE_FETCHED:
            return {
                ...state,
                data: action.payload,
            };
        default:
            return state;
    }
}
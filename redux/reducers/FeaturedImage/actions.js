import config from "../../../config";
import axios from "axios";
import { FEATURED_IMAGE_FETCHED, FEATURED_IMAGE_FETCH_FAILED } from "./actionType";

export function fetchFeaturedImage(payload) {
    return {
        type: FEATURED_IMAGE_FETCHED,
        payload
    }
}


export function fetchFeaturedImageFailed(payload) {
    return {
        type: FEATURED_IMAGE_FETCH_FAILED,
        payload
    }
}


export const fetchFeaturedImageData = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getFeaturedImage`,
         { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } })
        .then((res) => {
            if (res.data.success === true) {
                dispatch(fetchFeaturedImage(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};
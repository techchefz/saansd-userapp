import config from "../../../config";
import axios from "axios";
import { JOURNEY_DATA_FETCHED, JOURNEY_DATA_FETCH_FAILED } from "./actionType";

export function fetchJourney(payload) {
    return {
        type: JOURNEY_DATA_FETCHED,
        payload
    }
}


export function fetchJourneyFailed(payload) {
    return {
        type: JOURNEY_DATA_FETCH_FAILED,
        payload
    }
}


export const fetchJourneyData = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getJourney`,
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } }).then((res) => {
            if (res.data.success === true) {
                dispatch(fetchJourney(res.data.data[0]))
            }
        }).catch((err) => {
            console.log(err);
        });
};
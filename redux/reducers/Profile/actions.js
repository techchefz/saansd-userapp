import config from "../../../config";
import axios from "axios";
import { PROFILE_DATA_FETCHED, PROFILE_DATA_FETCH_FAILED } from "./actionType";

export function fetchProfile(payload) {
    return {
        type: PROFILE_DATA_FETCHED,
        payload
    }
}


export function fetchProfileFailed(payload) {
    return {
        type: PROFILE_DATA_FETCH_FAILED,
        payload
    }
}


export const fetchProfileData = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getProfile`,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } })
        .then((res) => {
            if (res.data.success === true) {
                dispatch(fetchProfile(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};
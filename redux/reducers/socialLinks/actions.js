import config from "../../../config";
import axios from "axios";
import { SOCIAL_LINK_FETCHED, SOCIAL_LINK_FETCH_FAILED } from "./actionType";

export function socialLink(payload) {
    return {
        type: SOCIAL_LINK_FETCHED,
        payload
    }
}


export function socialLinkFailed(payload) {
    return {
        type: SOCIAL_LINK_FETCH_FAILED,
        payload
    }
}


export const socialLinkData = () => {
    const userId = config.userId
    return (dispatch, getState) =>
        axios.get(`${config.serverUrl}:${config.port}/node/api/users/getsocialLink`,
         { headers: { 'Content-Type': 'application/x-www-form-urlencoded', userId } })
        .then((res) => {
            if (res.data.success === true) {
                dispatch(socialLink(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};